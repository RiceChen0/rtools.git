## RTool

RTools(米饭工具集)是开发工具集桌面悬浮窗软件，每一个子功能如同一颗米粒组成一碗米饭。

### RLOG工具呈现桌面悬浮窗和系统托盘
- 悬浮窗：鼠标右击，便可以浏览到目前RTool支持的工具。目前已开发RJlink Tool

![](./pic/Suspension_window.png)

- 系统托盘：鼠标右击，便可以退出应用

![](./pic/system_tray.png)

### Linux 编译及安装

- Arch Linux 通过 AUR 仓库安装 [rtools 开发版](https://aur.archlinux.org/packages/rtools-git)

```bash
yay -S rtools-git
```

注：开发板存在一些未知问题，等稳定后，可以安装发行版。

```bash
yay -S rtools
```

注：发行版暂时未打包。

- 其他 Linux 参考 AUR 的 PKGBUILD 文件自行编译
