﻿#ifndef WEBSERVERTOOL_H
#define WEBSERVERTOOL_H

#include <QMainWindow>
#include <QSettings>
#include <QFileDialog>

#include "httpserver.h"
#include "httplistener.h"

namespace Ui {
class WebServerTool;
}

class WebServerTool : public QMainWindow
{
    Q_OBJECT

public:
    explicit WebServerTool(QWidget *parent = nullptr);
    ~WebServerTool();

private slots:
    void on_buttonHttpBrowse_clicked();
    void on_buttonHttpHighSet_clicked();
    void on_buttonHttpStart_clicked();
    void on_buttonHttpAbout_clicked();
    void on_buttonHttpRestart_clicked();
    void on_buttonHttpViewConnect_clicked();
    void on_buttonHttpClearList_clicked();

    void on_checkBoxHttps_stateChanged(int arg1);

private:
    void httpStartListener();
    void httpStopListener();

private:
    QSettings *listenerSettings = nullptr;
    HttpServer *httpServer = nullptr;
    HttpListener *httpListener = nullptr;

private:
    Ui::WebServerTool *ui;
};

#endif // WEBSERVERTOOL_H
