﻿#include "httpserver.h"
#include "QDir"

HttpServer::HttpServer(QObject* parent)
    : HttpRequestHandler(parent)
{
    Q_UNUSED(parent)
}

HttpServer::HttpServer(QString path)
{
    basePath = path;
}


void HttpServer::service(HttpRequest &request, HttpResponse &response)
{

    QFile file(basePath + request.getPath());
    if(file.open(QFile::ReadOnly))
    {
        response.setHeader("Content-Type", "application/octet-stream");
        while (!file.atEnd() && !file.error())
        {
            QByteArray buffer=file.readAll();
            response.write(buffer);
        }
    }
    else
    {
        response.setStatus(404, "File not found");
    }
}

