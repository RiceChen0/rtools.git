﻿#include "webservertool.h"
#include "ui_webservertool.h"

WebServerTool::WebServerTool(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WebServerTool)
{
    ui->setupUi(this);

    listenerSettings = new QSettings(":/HttpServer/httpServer.ini", QSettings::IniFormat, nullptr);
    listenerSettings->beginGroup("listener");
}

WebServerTool::~WebServerTool()
{
    delete ui;
}

void WebServerTool::httpStartListener()
{
    if(httpListener != nullptr)
    {
        httpListener->close();
        delete httpListener;
        httpListener = nullptr;
    }

    if(ui->lineEditHttpPath->text().isEmpty())
    {
        ui->textBrowserHttpLog->append("请选择http服务目录");
        return;
    }

    listenerSettings->setValue("host", ui->comboBoxHttpIp->currentText());
    if(ui->checkBoxHttps->isChecked())
    {
        listenerSettings->setValue("sslKeyFile", "ssl/server.key");
        listenerSettings->setValue("sslCertFile", "ssl/server.crt");
        listenerSettings->setValue("caCertFile", "ssl/ca.crt");
        listenerSettings->setValue("verifyPeer", "false");
        listenerSettings->setValue("port", ui->lineEditHttpsPort->text());
    }
    else
    {
        listenerSettings->setValue("port", ui->lineEditHttpPort->text());
    }

    httpServer = new HttpServer(ui->lineEditHttpPath->text());
    httpListener = new HttpListener(listenerSettings, httpServer, nullptr);
    if(httpListener == nullptr)
    {
        ui->textBrowserHttpLog->append("Http 启动监听失败");
    }
    else
    {
        ui->textBrowserHttpLog->append("Http 启动监听成功");
    }
    ui->buttonHttpStart->setText("停止(Stop)");
    ui->lineEditHttpPath->setEnabled(false);
    ui->buttonHttpBrowse->setEnabled(false);
    ui->buttonHttpHighSet->setEnabled(false);
    ui->comboBoxHttpIp->setEnabled(false);
    ui->lineEditHttpPort->setEnabled(false);
    ui->lineEditHttpsPort->setEnabled(false);
    ui->buttonHttpRestart->setEnabled(true);
    ui->buttonHttpViewConnect->setEnabled(true);
    ui->buttonHttpClearList->setEnabled(true);
}

void WebServerTool::httpStopListener()
{
    if(httpListener != nullptr)
    {
        httpListener->close();
        delete httpListener;
        httpListener = nullptr;
    }

    ui->buttonHttpStart->setText("启动(Start)");
    ui->textBrowserHttpLog->append("Http 关闭监听");
    ui->lineEditHttpPath->setEnabled(true);
    ui->buttonHttpBrowse->setEnabled(true);
    ui->buttonHttpHighSet->setEnabled(true);
    ui->comboBoxHttpIp->setEnabled(true);
    ui->lineEditHttpPort->setEnabled(true);
    ui->lineEditHttpsPort->setEnabled(true);
    ui->buttonHttpRestart->setEnabled(false);
    ui->buttonHttpViewConnect->setEnabled(false);
    ui->buttonHttpClearList->setEnabled(false);
}

void WebServerTool::on_buttonHttpBrowse_clicked()
{
    QString path=QFileDialog::getExistingDirectory(this,"WebServer Tool");
    ui->lineEditHttpPath->setText(path);
}

void WebServerTool::on_buttonHttpHighSet_clicked()
{

}

void WebServerTool::on_buttonHttpStart_clicked()
{
    if(ui->buttonHttpStart->text() == tr("启动(Start)"))
    {
        httpStartListener();
    }
    else
    {
        httpStopListener();
    }
}

void WebServerTool::on_buttonHttpAbout_clicked()
{

}

void WebServerTool::on_buttonHttpRestart_clicked()
{
    httpStopListener();
    httpStartListener();
}

void WebServerTool::on_buttonHttpViewConnect_clicked()
{

}

void WebServerTool::on_buttonHttpClearList_clicked()
{

}

void WebServerTool::on_checkBoxHttps_stateChanged(int arg1)
{
    Q_UNUSED(arg1);

    if(ui->checkBoxHttps->isChecked())
    {
        ui->lineEditHttpPort->setEnabled(false);
    }
    else
    {
        ui->lineEditHttpPort->setEnabled(true);
    }
}

