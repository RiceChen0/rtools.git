﻿#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include "httprequesthandler.h"

using namespace stefanfrings;

class HttpServer : public HttpRequestHandler
{
    Q_OBJECT
public:
    HttpServer(QObject* parent=nullptr);
    HttpServer(QString path);

    void service(HttpRequest& request, HttpResponse& response);

private:
    QString basePath;

};

#endif // HTTPSERVER_H
