#ifndef SERIALTOOL_H
#define SERIALTOOL_H

#include <QWidget>

namespace Ui {
class SerialTool;
}

class SerialTool : public QWidget
{
    Q_OBJECT

public:
    explicit SerialTool(QWidget *parent = nullptr);
    ~SerialTool();

private:
    Ui::SerialTool *ui;
};

#endif // SERIALTOOL_H
