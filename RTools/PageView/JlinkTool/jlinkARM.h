﻿#ifndef JLINKARMH
#define JLINKARMH

//JLINK TIF
#define JLINKARM_TIF_JTAG                0
#define JLINKARM_TIF_SWD                 1
#define JLINKARM_TIF_DBM3                2
#define JLINKARM_TIF_FINE                3
#define JLINKARM_TIF_2wire_JTAG_PIC32	 4

//RESET TYPE
#define JLINKARM_RESET_TYPE_NORMAL       0
#define JLINKARM_RESET_TYPE_CORE         1
#define JLINKARM_RESET_TYPE_PIN          2

typedef bool  (*jlinkOpenFunc)(void);
typedef void  (*jlinkCloseFunc)(void);
typedef bool  (*jlinkIsOpenFunc)(void);
typedef unsigned int (*jlinkTIFSelectFunc)(int);
typedef void  (*jlinkSetSpeedFunc)(int);
typedef unsigned int (*jlinkGetSpeedFunc)(void);
typedef void  (*jlinkResetFunc)(void);
typedef int   (*jlinkHaltFunc)(void);
typedef void  (*jlinkGoFunc)(void);

typedef int   (*jlinkReadMemFunc)(unsigned int addr, int len, void *buf);
typedef int   (*jlinkWriteMemFunc)(unsigned int addr, int len, void *buf);
typedef int   (*jlinkWriteU8Func)(unsigned int addr, unsigned char data);
typedef int   (*jlinkWriteU16Func)(unsigned int addr, unsigned short data);
typedef int   (*jlinkWriteU32Func)(unsigned int addr, unsigned int data);

typedef int   (*jlinkEraseChipFunc)(void);
typedef int   (*jlinkDownloadFileFunc)(const char *file, unsigned int addr);
typedef void  (*jlinkBeginDownloadFunc)(int index);
typedef void  (*jlinkEndDownloadFunc)(void);
typedef bool  (*jlinkExecCommandFunc)(const char* cmd, int a, int b);

typedef unsigned int (*jlinkReadRegFunc)(int index);
typedef int   (*jlinkWriteRegFunc)(int index, unsigned int data);

typedef void  (*jlinkSetLogFileFunc)(char *file);
typedef unsigned int (*jlinkGetDLLVersionFunc)(void);
typedef unsigned int (*jlinkGetHardwareVersionFunc)(void);
typedef unsigned int (*jlinkGetFirmwareStringFunc)(char *buff, int count);
typedef unsigned int (*jlinkGetSNFunc)(void);
typedef unsigned int (*jlinkGetIdFunc)(void);
typedef bool  (*jlinkConnectFunc)(void);
typedef bool  (*jlinkIsConnectedFunc)(void);

#endif // JLINKARMH
