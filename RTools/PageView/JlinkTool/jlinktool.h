﻿#ifndef JLINKTOOL_H
#define JLINKTOOL_H

#include <QWidget>
#include <QFileDialog>
#include <QSettings>
#include <QLibrary>
#include <QDateTime>
#include <QTimer>

#include "jlinkARM.h"

#define JLINK_PARAMETER_SAVA_FILE                   "JlinkParammter.ini"
#define JLINK_DEFAULT_BURN_ADDR                     "0x08000000"

#define JLINK_BURN_TIME_INTERVAL                    5
#define JLINK_BURN_CONTENT_SIZE                     1024

namespace Ui {
class JlinkTool;
}

class JlinkTool : public QWidget
{
    Q_OBJECT

public:
    explicit JlinkTool(QWidget *parent = nullptr);
    ~JlinkTool();

private slots:
    void on_buttonSelectFW_clicked();
    void on_buttonClearShow_clicked();
    void on_buttonGetID_clicked();
    void on_buttoEraseFlash_clicked();
    void on_buttonBurn_clicked();
    void on_lineEditAddr_textChanged(const QString &arg1);

    void burnFileTimerHandle(void);
private:
    jlinkOpenFunc jlinkOpenFuncPtr = NULL;
    jlinkCloseFunc jlinkCloseFuncPtr = NULL;
    jlinkIsOpenFunc jlinkIsOpenFuncPtr = NULL;
    jlinkTIFSelectFunc jlinkTIFSelectFuncPtr = NULL;
    jlinkSetSpeedFunc jlinkSetSpeedFuncPtr = NULL;
    jlinkGetSpeedFunc jlinkGetSpeedFuncPtr = NULL;
    jlinkResetFunc jlinkResetFuncPtr = NULL;
    jlinkHaltFunc jlinkHaltFuncPtr = NULL;
    jlinkGoFunc jlinkGoFuncPtr = NULL;
    jlinkReadMemFunc jlinkReadMemFuncPtr = NULL;
    jlinkWriteMemFunc jlinkWriteMemFuncPtr = NULL;
    jlinkWriteU8Func jlinkWriteU8FuncPtr = NULL;
    jlinkWriteU16Func jlinkWriteU16FuncPtr = NULL;
    jlinkWriteU32Func jlinkWriteU32FuncPtr = NULL;
    jlinkEraseChipFunc jlinkEraseChipFuncPtr = NULL;
    jlinkDownloadFileFunc jlinkDownloadFileFuncPtr = NULL;
    jlinkBeginDownloadFunc jlinkBeginDownloadFuncPtr = NULL;
    jlinkEndDownloadFunc jlinkEndDownloadFuncPtr = NULL;
    jlinkExecCommandFunc jlinkExecCommandFuncPtr = NULL;
    jlinkReadRegFunc jlinkReadRegFuncPtr = NULL;
    jlinkWriteRegFunc jlinkWriteRegFuncPtr = NULL;
    jlinkSetLogFileFunc jlinkSetLogFileFuncPtr = NULL;
    jlinkGetDLLVersionFunc jlinkGetDLLVersionFuncPtr = NULL;
    jlinkGetHardwareVersionFunc jlinkGetHardwareVersionFuncPtr = NULL;
    jlinkGetFirmwareStringFunc jlinkGetFirmwareStringFuncPtr = NULL;
    jlinkGetSNFunc jlinkGetSNFuncPtr = NULL;
    jlinkGetIdFunc jlinkGetIdFuncPtr = NULL;
    jlinkConnectFunc jlinkConnectFuncPtr = NULL;
    jlinkIsConnectedFunc jlinkIsConnectedFuncPtr = NULL;

private:
    void jlinkLibLoadHandle(void);
    bool jlinkConnectHandle(void);
    void jlinkdisconnectHandle(void);
    QString jlinkGetCpuIdHandle(void);

    bool jlinkOpen(void);
    void jlinkClose(void);
    bool jlinkIsOpen(void);
    unsigned int jlinkTIFSelect(int);
    void jlinkSetSpeed(unsigned int);
    unsigned int jlinkGetSpeed(void);
    void jlinkReset(void);
    int jlinkHalt(void);
    int jlinkReadMem(unsigned int addr, int len, void *buf);
    int jlinkWriteMem(unsigned int addr, int len, void *buf);
    int jlinkEraseChip(void);
    bool jlinkExecCommand(const char *cmd, int a, int b);
    unsigned int jlinkGetDLLVersion(void);
    unsigned int jlinkGetSN(void);
    unsigned int jlinkGetId(void);
    bool jlinkConnect(void);
    bool jlinkIsConnected(void);

private:
    void getParamHandle(void);
    void setParamHandle(void);
    void infoShowHandle(QString info);

private:
    Ui::JlinkTool *ui;

    QLibrary *jlinkLib;
    unsigned int burnAddr;
    unsigned int burnFileSize;
    QByteArray burnFileContent;
    QTimer *burnFileTimer;
};

#endif // JLINKTOOL_H
