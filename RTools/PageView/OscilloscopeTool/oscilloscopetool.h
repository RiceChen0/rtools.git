#ifndef OSCILLOSCOPETOOL_H
#define OSCILLOSCOPETOOL_H

#include <QWidget>
#include "qcustomplot.h"
#include <QTcpSocket>
#include <QTcpServer>
#include <QSerialPort>
#include <QSerialPortInfo>

namespace Ui {
class OscilloscopeTool;
}

#define OSC_CHANNEL_MAX     4

class OscilloscopeTool : public QWidget
{
    Q_OBJECT

    typedef struct
    {
        uint8_t head1;
        uint8_t head2;
        float data[OSC_CHANNEL_MAX];
    }OscPacket;

public:
    explicit OscilloscopeTool(QWidget *parent = nullptr);
    ~OscilloscopeTool();

private slots:
    void on_buttonSerialOpen_clicked();
    void on_buttonNetOpen_clicked();
    void on_buttonClearData_clicked();
    void on_buttonH_VZoom_clicked();
    void on_buttonHorizontalZoom_clicked();
    void on_buttonVerticalZoom_clicked();
    void on_radioButtonSerial_clicked();
    void on_radioButtonNet_clicked();

    void serialRecv();

    void tcpConnect();
    void tcpRecv();
    void tcpDisconnect();

private:
    void waveWindowInit(void);
    void wavaWindowDraw(double ch1Data, double ch2Data, double ch3Data, double ch4Data);
    void waveDataWrite(double ch1Data, double ch2Data, double ch3Data, double ch4Data);

    uint8_t dataChecksum(float *data, uint16_t len);
    void dataProcessing(QByteArray array);

private:
    Ui::OscilloscopeTool *ui;

    QPointer<QCPGraph> graphCh1, graphCh2, graphCh3, graphCh4;

    QSerialPort serial;

    QTcpServer *tcpServer;
    QTcpSocket *serverSocket;
};

#endif // OSCILLOSCOPETOOL_H
