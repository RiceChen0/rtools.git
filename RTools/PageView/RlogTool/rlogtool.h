#ifndef RLOGTOOL_H
#define RLOGTOOL_H

#include <QWidget>

namespace Ui {
class RlogTool;
}

class RlogTool : public QWidget
{
    Q_OBJECT

public:
    explicit RlogTool(QWidget *parent = nullptr);
    ~RlogTool();

private:
    Ui::RlogTool *ui;
};

#endif // RLOGTOOL_H
