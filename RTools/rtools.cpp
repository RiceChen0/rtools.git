﻿#include "rtools.h"
#include "ui_rtools.h"
#include <QtWidgets>
#include "iconhelper.h"

RTools::RTools(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::RTools)
{
    ui->setupUi(this);

    systemTrayIcon = new QSystemTrayIcon(this);
    systemTrayIcon->setIcon(QIcon(":/image/logo.png"));
    systemTrayIcon->setToolTip(QObject::tr("RTools"));

    systemTrayMenu = new QMenu(this);
    systemTrayExitAction = new QAction(tr("Exit"), this);
    systemTrayExitAction->setIcon(QIcon(":/image/close.png"));
    systemTrayMenu->addAction(systemTrayExitAction);
    systemTrayIcon->setContextMenu(systemTrayMenu);

    connect(systemTrayExitAction, SIGNAL(triggered()), this, SLOT(closeWindowHandle()));
    systemTrayIcon->show();

    this->setWindowIcon(QIcon(":/image/logo.png"));

    initMainForms();
    initToolsItem();
}

RTools::~RTools()
{
    delete ui;
}

void RTools::initMainForms(void)
{
    this->setProperty("form", true);
    this->setProperty("canMove", true);
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);

    IconHelper::Instance()->setIcon(ui->labelLogo, QChar(RTOOLS_LOGO_BTN_ICON), 50);

    ui->widgetTitle->setProperty("form", "title");
    ui->labelTitle->setText("RTools");
    ui->labelTitle->setStyleSheet("font-size:32px");
    this->setWindowTitle(ui->labelTitle->text());

    IconHelper::Instance()->setIcon(ui->buttonMin, QChar(RTOOLS_MIN_BTN_ICON), 12);
    IconHelper::Instance()->setIcon(ui->buttonMax, QChar(RTOOLS_MAX_BTN_ICON), 12);
    IconHelper::Instance()->setIcon(ui->buttonExit, QChar(RTOOLS_EXIT_BTN_ICON), 12);

    ui->widgetController->setProperty("flag", "left");
    ui->pageController->setStyleSheet(QString("QWidget[flag=\"left\"] "
                                              "QAbstractButton{min-height:%1px;max-height:%1px;}").arg(RTOOLS_ITEM_HEIGH));

}

void RTools::initToolsItem(void)
{
    QList<int> pixIconChar;
    pixIconChar << RTOOLS_WEBSERVER_BTN_ICON << RTOOLS_SERIAL_BTN_ICON << RTOOLS_JLINK_BTN_ICON << RTOOLS_RLOG_BTN_ICON << RTOOLS_OSCILLOSCOPE_BTN_ICON << RTOOLS_PLAYER_BTN_ICON;

    buttonItems << ui->buttonWebServer << ui->buttonSerial << ui->buttonJlink << ui->buttonRlog << ui->buttonOscilloscope << ui->buttonPlayer;

    for (int i = 0; i < buttonItems.count(); i++) {
        buttonItems.at(i)->setStyleSheet("font-size:13px");
        buttonItems.at(i)->setCheckable(true);
        buttonItems.at(i)->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        connect(buttonItems.at(i), SIGNAL(clicked()), this, SLOT(buttonItem_clicked()));
    }

    IconHelper::Instance()->setStyle(ui->widgetController, buttonItems,
                                     pixIconChar, RTOOLS_ITEM_ICON_SIZE, RTOOLS_ITEM_ICON_WIDTH, RTOOLS_ITEM_ICON_HEIGH, "left", 4);

    buttonItems.at(0)->click();
}

void RTools::buttonItem_clicked()
{
    QToolButton *currentBtn = (QToolButton *)sender();
    int item = 0;

    for (int index = 0; index < buttonItems.count(); index++)
    {
        if (buttonItems.at(index) == currentBtn)
        {
            buttonItems.at(index)->setChecked(true);
            buttonItems.at(index)->setIcon(QIcon(IconHelper::Instance()->getPixmap(buttonItems.at(index), false)));
            item = index;
        }
        else
        {
            buttonItems.at(index)->setChecked(false);
            buttonItems.at(index)->setIcon(QIcon(IconHelper::Instance()->getPixmap(buttonItems.at(index), true)));
        }
    }

    ui->stackedWidget->setCurrentIndex(item);
}

void RTools::closeWindowHandle()
{
    exit(0);
}

void RTools::on_buttonMin_clicked()
{
    this->showMinimized();
}

void RTools::on_buttonMax_clicked()
{
    static bool max = false;
    static QRect location = this->geometry();

    if (max) {
        this->setGeometry(location);
        IconHelper::Instance()->setIcon(ui->buttonMax, QChar(0xe63c), 12);
        ui->buttonMax->setToolTip("最大化");
    } else {
        location = this->geometry();
        QRect mRect = QGuiApplication::primaryScreen()->availableGeometry();
        this->setGeometry(mRect);
        IconHelper::Instance()->setIcon(ui->buttonMax, QChar(0xe600), 12);
        ui->buttonMax->setToolTip("向下还原");
    }

    this->setProperty("canMove", max);
    max = !max;
}

void RTools::on_buttonExit_clicked()
{

}

