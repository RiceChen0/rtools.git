#ifndef RTOOLFORM_H
#define RTOOLFORM_H

#include <QObject>

#if (QT_VERSION > QT_VERSION_CHECK(5,0,0))
#include <QtWidgets>
#endif

class RToolForm : public QObject
{
    Q_OBJECT
public:
    static RToolForm *Instance();
    explicit RToolForm(QObject *parent = 0);

    void start();

protected:
    bool eventFilter(QObject *obj, QEvent *evt);

signals:
    void mouseDoubleClick(void);
};

#endif // RTOOLFORM_H
