#include "rtoolform.h"

RToolForm *RToolForm::Instance()
{
    static RToolForm *self = NULL;
    if (!self) {
        QMutex mutex;
        QMutexLocker locker(&mutex);
        if (!self) {
            self = new RToolForm;
        }
    }

    return self;
}

RToolForm::RToolForm(QObject *parent)
    : QObject(parent)
{

}

bool RToolForm::eventFilter(QObject *obj, QEvent *evt)
{
    QWidget *w = (QWidget *)obj;
    static QPoint mousePoint;
    static bool mousePressed = false;

    if (!w->property("canMove").toBool()) {
        return QObject::eventFilter(obj, evt);
    }

    QMouseEvent *event = static_cast<QMouseEvent *>(evt);
    if (event->type() == QEvent::MouseButtonPress) {
        if (event->button() == Qt::LeftButton) {
            mousePressed = true;
            mousePoint = event->globalPos() - w->pos();
            return true;
        }
    } else if (event->type() == QEvent::MouseButtonRelease) {
        mousePressed = false;
        return true;
    } else if (event->type() == QEvent::MouseMove) {
        if (mousePressed && (event->buttons() & Qt::LeftButton)) {
            w->move(event->globalPos() - mousePoint);
            return true;
        }
    } else if (event->type() == QEvent::MouseButtonDblClick) {
        emit mouseDoubleClick();
    }

    return QObject::eventFilter(obj, evt);
}

void RToolForm::start()
{
    qApp->installEventFilter(this);
}
