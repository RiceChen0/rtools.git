QT       += core gui network printsupport serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

CONFIG(debug, debug|release) {
    DEFINES += SUPERVERBOSE
}

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Widgets/ComboBoxSerialPort/comboboxserialport.cpp \
    component/HttpServer/httpconnectionhandler.cpp \
    component/HttpServer/httpconnectionhandlerpool.cpp \
    component/HttpServer/httpcookie.cpp \
    component/HttpServer/httpglobal.cpp \
    component/HttpServer/httplistener.cpp \
    component/HttpServer/httprequest.cpp \
    component/HttpServer/httprequesthandler.cpp \
    component/HttpServer/httpresponse.cpp \
    component/HttpServer/httpsession.cpp \
    component/HttpServer/httpsessionstore.cpp \
    component/HttpServer/staticfilecontroller.cpp \
    component/IconHelper/iconhelper.cpp \
    PageView/JlinkTool/jlinktool.cpp \
    PageView/RlogTool/rlogtool.cpp \
    PageView/SerialTool/serialtool.cpp \
    PageView/WebServerTool/httpserver.cpp \
    PageView/WebServerTool/webservertool.cpp \
    PageView/OscilloscopeTool/oscilloscopetool.cpp \
    Widgets/ComboBoxIP/comboboxip.cpp \
    Widgets/QCustomPlot/qcustomplot.cpp \
    main.cpp \
    rtoolform.cpp \
    rtools.cpp

HEADERS += \
    Widgets/ComboBoxSerialPort/comboboxserialport.h \
    component/HttpServer/httpconnectionhandler.h \
    component/HttpServer/httpconnectionhandlerpool.h \
    component/HttpServer/httpcookie.h \
    component/HttpServer/httpglobal.h \
    component/HttpServer/httplistener.h \
    component/HttpServer/httprequest.h \
    component/HttpServer/httprequesthandler.h \
    component/HttpServer/httpresponse.h \
    component/HttpServer/httpsession.h \
    component/HttpServer/httpsessionstore.h \
    component/HttpServer/staticfilecontroller.h \
    component/IconHelper/iconhelper.h \
    PageView/JlinkTool/jlinkARM.h \
    PageView/JlinkTool/jlinktool.h \
    PageView/RlogTool/rlogtool.h \
    PageView/SerialTool/serialtool.h \
    PageView/WebServerTool/httpserver.h \
    PageView/WebServerTool/webservertool.h \
    PageView/OscilloscopeTool/oscilloscopetool.h \
    Widgets/QCustomPlot/qcustomplot.h \
    Widgets/ComboBoxIP/comboboxip.h \
    rtoolform.h \
    rtools.h

INCLUDEPATH +=  \
    $$PWD/component/IconHelper  \
    $$PWD/component/HttpServer  \
    $$PWD/Widgets/ComboBoxIP    \
    $$PWD/Widgets/QCustomPlot   \
    $$PWD/PageView/SerialTool   \
    $$PWD/PageView/JlinkTool    \
    $$PWD/PageView/RlogTool     \
    $$PWD/PageView/WebServerTool


FORMS += \
    PageView/JlinkTool/jlinktool.ui \
    PageView/OscilloscopeTool/oscilloscopetool.ui \
    PageView/RlogTool/rlogtool.ui \
    PageView/SerialTool/serialtool.ui \
    PageView/WebServerTool/webservertool.ui \
    rtools.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource/httpserver.qrc \
    resource/image.qrc \
    resource/qss.qrc

DISTFILES += \
    component/HttpServer/httpserver.pri
