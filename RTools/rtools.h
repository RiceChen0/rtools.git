﻿#ifndef RTOOLS_H
#define RTOOLS_H

#include <QMainWindow>
#include <QToolButton>
#include <QSystemTrayIcon>

#include "serialtool.h"
#include "jlinktool.h"

QT_BEGIN_NAMESPACE
namespace Ui { class RTools; }
QT_END_NAMESPACE

class RTools : public QMainWindow
{
    Q_OBJECT

#define RTOOLS_LOGO_BTN_ICON                0xe60d

#define RTOOLS_WEBSERVER_BTN_ICON           0xeb8e
#define RTOOLS_SERIAL_BTN_ICON              0xe7e6
#define RTOOLS_JLINK_BTN_ICON               0xe60e
#define RTOOLS_RLOG_BTN_ICON                0xe715
#define RTOOLS_OSCILLOSCOPE_BTN_ICON        0xe681
#define RTOOLS_PLAYER_BTN_ICON              0xe681

#define RTOOLS_MIN_BTN_ICON                 0xe63b
#define RTOOLS_MAX_BTN_ICON                 0xe63c
#define RTOOLS_EXIT_BTN_ICON                0xe62b

#define RTOOLS_ITEM_HEIGH                   32
#define RTOOLS_ITEM_ICON_SIZE               18
#define RTOOLS_ITEM_ICON_WIDTH              35
#define RTOOLS_ITEM_ICON_HEIGH              25

    enum RTOOLS_ITEM {
        RTOOLS_ITEM_WEBSERVER,
        RTOOLS_ITEM_SERIAL,
        RTOOLS_ITEM_JLINK,
        RTOOLS_ITEM_MAX,
    };

public:
    RTools(QWidget *parent = nullptr);
    ~RTools();

private slots:
    void on_buttonMin_clicked();
    void on_buttonMax_clicked();
    void on_buttonExit_clicked();
    void buttonItem_clicked();
    void closeWindowHandle();

private:
    Ui::RTools *ui;

private:
    void initMainForms(void);
    void initToolsItem(void);
    void itemPageDisplay(RTOOLS_ITEM item);

    QList<QToolButton *> buttonItems;

    QSystemTrayIcon *systemTrayIcon = nullptr;
    QMenu *systemTrayMenu = nullptr;
    QAction *systemTrayExitAction = nullptr;

    SerialTool *serialTool = nullptr;
    JlinkTool *jlinkTool = nullptr;
};
#endif // RTOOLS_H
