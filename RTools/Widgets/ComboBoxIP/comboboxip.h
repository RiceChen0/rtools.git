#ifndef QCOMBOBOXIP_H
#define QCOMBOBOXIP_H

#include <QComboBox>
#include <QMouseEvent>
#include <QHostInfo>
#include <QNetworkInterface>

class ComboBoxIP : public QComboBox
{
    Q_OBJECT
public:
    explicit ComboBoxIP(QWidget *parent = nullptr);

protected:
    void mousePressEvent(QMouseEvent *event);

public Q_SLOTS:
    void currentIpChanged(const QString &ip);

signals:

private:
    void ScanActiveIP(void);
    void ShowHistory(void);
};

#endif // QCOMBOBOXIP_H
