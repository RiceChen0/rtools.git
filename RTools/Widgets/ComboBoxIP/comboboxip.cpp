#include "comboboxip.h"

ComboBoxIP::ComboBoxIP(QWidget *parent) : QComboBox(parent)
{
    ScanActiveIP();
    connect(this, SIGNAL(currentTextChanged(const QString &)), this, SLOT(currentIpChanged(const QString &)));
}

void ComboBoxIP::ScanActiveIP()
{
    clear();

    QList<QHostAddress> localIpAddrList = QNetworkInterface::allAddresses();
    foreach(QHostAddress localIpAddr, localIpAddrList) {
        if(localIpAddr.protocol() == QAbstractSocket::IPv4Protocol) {
            this->addItem(localIpAddr.toString());
        }
    }
}

void ComboBoxIP::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton) {
        ScanActiveIP();
        showPopup();
    }
}

void ComboBoxIP::currentIpChanged(const QString &ip)
{
    if (ip.size() == 0) {
        return;
    }
}
