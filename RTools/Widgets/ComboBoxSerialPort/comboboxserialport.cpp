#include "comboboxserialport.h"

ComboBoxSerialPort::ComboBoxSerialPort(QWidget *parent) : QComboBox(parent)
{
    scanActiveSerialPort();
}

void ComboBoxSerialPort::scanActiveSerialPort(void)
{
    QStringList portNameList;

    clear();

    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        QString serialPortInfo = info.portName() + ":" + info.description();
        portNameList << serialPortInfo;
    }
    this->addItems(portNameList);
}

void ComboBoxSerialPort::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        scanActiveSerialPort();
        showPopup();
    }
}

