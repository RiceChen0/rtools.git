#ifndef QCOMBOBOXSERIALPORT_H
#define QCOMBOBOXSERIALPORT_H

#include <QComboBox>
#include <QMouseEvent>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

class ComboBoxSerialPort : public QComboBox
{
    Q_OBJECT
public:
    explicit ComboBoxSerialPort(QWidget *parent = nullptr);

protected:
    void mousePressEvent(QMouseEvent *event);

signals:

private:
    void scanActiveSerialPort(void);
};

#endif // QCOMBOBOXSERIALPORT_H
